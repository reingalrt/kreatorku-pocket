import { Network } from '@ionic-native/network';
import { Component } from '@angular/core';
import { Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { timer } from 'rxjs/observable/timer';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import {Storage} from '@ionic/storage';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:string = 'IntroSliderPage';

  showSplash = true;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private nativePageTransitions: NativePageTransitions, private network: Network, private toastCtrl: ToastController, private storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      

      timer(3000).subscribe(() => this.showSplash = false);

      network.onConnect().subscribe(() => {
        storage.set('conn', true);
        let conoff = toastCtrl.create({
            closeButtonText: 'Ok',
            showCloseButton: true,
            message: 'Youre not connected to internet.',
            position: 'top'
        });
    
        conoff.present();
    });
    });
  }
}

