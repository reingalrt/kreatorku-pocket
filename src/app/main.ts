import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import {Network} from '@ionic-native/network';

platformBrowserDynamic().bootstrapModule(AppModule);
