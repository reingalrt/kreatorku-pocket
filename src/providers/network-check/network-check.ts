import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, Platform } from 'ionic-angular'

/*
  Generated class for the NetworkCheckProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkCheckProvider {

  constructor(public toast: ToastController, public storage: Storage, public platform: Platform) {
  }

  verifyConnection = () : Promise<boolean> => {
    return new Promise<boolean>((res, rej) => {
      this.storage.get('conn').then(conn => {
        if(conn){
          res(true);
        }
        else{
          let t = this.toast.create({
            closeButtonText: 'Ok',
            showCloseButton: true,
            message: 'Kamu tidak terhubung ke internet :( coba nanti lagi ya.',
            position: 'bottow'
          });
          t.present();
          res(false);
        }
      })
    })
  }

}
