import { LoginPage } from './../login/login';
import { DashboardPage } from './../dashboard/dashboard';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast, ToastController, LoadingController } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';
import { Connection } from '@angular/http';
import { Network } from '@ionic-native/network'
import { NetworkCheckProvider } from '../../providers/network-check/network-check'

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  loading: any;
  registerData = {"username":"", "email":"", "password":""}

  constructor(public loadingCtrl: LoadingController ,public navCtrl: NavController, public navParams: NavParams, public restServiceProvider: RestServiceProvider, public network: Network, public toastCtrl: ToastController, public verif: NetworkCheckProvider) {
  }

  ionViewDidLoad() {
    console.log("hello world");
  }

  register(){
    this.showLoader();
    this.restServiceProvider.register(this.registerData).then(
    (result) => {
      this.loading.dismiss();
      this.navCtrl.push(DashboardPage);
    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
    });
  }

  goToLogin(){
    this.navCtrl.push(LoginPage);
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Mengotentifikasi...'
    });
    this.loading.present();
  }

  presentToast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

}
