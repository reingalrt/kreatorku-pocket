import { Network } from '@ionic-native/network';
import { DashboardPage } from './../dashboard/dashboard';
import { Component } from '@angular/core';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

///TODO: konfigurasi akses api untuk login

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loading: any;
  loginData = {"username":"", "password":""};

  constructor(public navCtrl: NavController, public navParams: NavParams, public restService: RestServiceProvider, public restServiceProvider: RestServiceProvider, public network: Network, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    this.showLoader();
    this.restServiceProvider.login(this.loginData).then((result) => {
      this.loading.dismiss();
      this.navCtrl.push(DashboardPage);
    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
    });
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Mengotentifikasi...'
    });
    this.loading.present();
  }

  presentToast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

}
