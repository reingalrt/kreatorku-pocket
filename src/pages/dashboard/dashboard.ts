import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HeaderColor } from '@ionic-native/header-color';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public headerColor: HeaderColor) {
  }

  ionViewDidLoad() {
    this.headerColor.tint('#da2868');
    console.log('ionViewDidLoad DashboardPage');
  }

}
